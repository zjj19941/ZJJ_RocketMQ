/*
 Navicat Premium Data Transfer

 Source Server         : zjj101
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : zjj101:3306
 Source Schema         : delay_order

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 04/05/2021 14:08:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for order_exp
-- ----------------------------
DROP TABLE IF EXISTS `order_exp`;
CREATE TABLE `order_exp`  (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `order_no` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '订单号',
  `order_note` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '订单的备注信息',
  `insert_time` datetime(0) NULL DEFAULT NULL COMMENT '订单的插入时间',
  `expire_duration` bigint(22) NULL DEFAULT NULL COMMENT '订单的过期时长,单位秒',
  `expire_time` datetime(0) NULL DEFAULT NULL COMMENT '订单的过期时间',
  `order_status` smallint(6) NULL DEFAULT NULL COMMENT '订单的状态 0:未支付 1:已支付 -1:已过期,关闭 ',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
